from praat import app

# Run server on port 5000
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
