from praat import app

# Run server on port 80
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
